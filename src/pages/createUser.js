import React from 'react-native';

const {
    Text,
    StyleSheet,
    TouchableHighlight,
    View,
} = React;

import NavigationBar from 'react-native-navbar';
import t from 'tcomb-form-native';
import Actions from '../actions';
import PageBase from '../utils/PageBase';

var Form = t.form.Form;

var User = t.struct({
    account : t.String,
    pwd : t.String,
    name : t.String
});

var options = {
    fileds : {
        account : {
            value : 'test'
        },
        pwd : {
            value:'123456'
        },
        name : {
            value : 'tomas'
        }
    }
}

export default class CreateUser extends PageBase {

    constructor(props){
        super(props);
        this.onPress = this.onPress.bind(this);
    }
    onPress(){
        var data = this.refs.form.getValue();
        if(data){
            Actions.Users.Createuser(data);
        }
    }
    render(){
        const leftButtonConfig = {
            title: '取消',
            handler: () => this.props.navigator.pop(),
        };

        return (
            <View>
                <View>
                    <NavigationBar
                    title={{ title: 'Title', }}
                    leftButton={leftButtonConfig} />
                </View>
                <Form
                    ref='form'
                    type={User}
                    options = {options}/>
                <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>註冊</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }
});
