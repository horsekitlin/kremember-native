import React from 'react-native';

const {
    Text,
    StyleSheet,
    TouchableHighlight,
    View,
} = React;

import NavigationBar from 'react-native-navbar';
import t from 'tcomb-form-native';
import Actions from '../actions';
import PageBase from '../utils/PageBase';
import CreateUser from './createUser';
import CustomScreen from './CustomScreen';
import FBLogin from 'react-native-facebook-login';
import { Notifier } from '../stores';

var Form = t.form.Form;

var Login = t.struct({
    account : t.String,
    pwd : t.String,
});

var options = {
    fileds : {
        pwd : {
            password : true
        }
    }
}

export default class Loginview extends PageBase {
    constructor(props){
        super(props);
        this.createUser = this.createUser.bind(this);
        this._reloadComponent = this._reloadComponent.bind(this);
        this.onPress = this.onPress.bind(this);
    }
    _reloadComponent(){
        this.props.navigator.push({
            component: CustomScreen,
        });
    }
    componentWillMount(){
        Notifier.addListener('LoginSuccess', this._reloadComponent);
    }
    componentWillUnmount(){
        Notifier.removeChangeListener(this._reloadComponent);
    }
    onPress(){
        var data = this.refs.form.getValue();
        if(data){
            Actions.Users.Login(data);
        }
    }
    createUser(){
        Actions.ClientSet.Redirect({
            view : 'createuser'
        });
    }
    render(){
        const rightButtonConfig = {
            title: '建立會員',
            handler: () => this.props.navigator.push({
                component: CreateUser,
            }),
        };

        return (
            <View>
                <View style={{ flex: 1, backgroundColor: '#ff9900', }}>
                    <NavigationBar
                    title={{ title: '登入', }}
                    rightButton={rightButtonConfig} />
                </View>
                <Form
                    ref='form'
                    type={Login}
                    options = {options}/>
                <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Save</Text>
                </TouchableHighlight>
                <View>
                <FBLogin style={{ marginBottom: 10, }}
                    permissions={["email","user_friends"]}
                    onLogin={function(data){
                        Actions.Users.FBLogin(data);
                    }.bind(this)}
                    onLogout={function(){
                        console.log("Logged out.");
                        this.setState({ user : null });
                    }.bind(this)}
                    onLoginFound={function(data){
                        console.log("Existing login found.");
                        console.log(data);
                        this.setState({ user : data.credentials });
                    }.bind(this)}
                    onLoginNotFound={function(){
                        console.log("No user logged in.");
                        this.setState({ user : null });
                    }.bind(this)}
                    onError={function(data){
                        console.log("ERROR");
                        console.log(data);
                    }}
                    onCancel={function(){
                        console.log("User cancelled.");
                    }}
                    onPermissionsMissing={function(data){
                        console.log("Check permissions!");
                        console.log(data);
                    }}
                    />
                </View>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }
});
