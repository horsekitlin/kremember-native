import React from 'react-native';
import { fetch_to } from '../utils/requests';

const {
  Component,
  Text,
  ListView,
  View
} = React;
import NavigationBar from 'react-native-navbar';
import MemberRow from '../components/MemberRow';
var RefreshableListView = require('react-native-refreshable-listview');

export default class CustomScreen extends Component {
    constructor(props){
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.renderArticle = this.renderArticle.bind(this);
        this.state = {
            members : ds.cloneWithRows(['row 1', 'row 2']),
        };
    }
    renderArticle(article) {
        console.log('reload');
    }
    render() {
        return (
        <View style={{ flex: 1, backgroundColor: '#9999CC', }}>
            <NavigationBar
            title={{ title: 'Member List', }}/>
            <View style={{ flex:1,borderWidth : 2, borderColor : '#00ff00',padding: 10}}>
                <RefreshableListView
                    style={{padding : 10}}
                    dataSource={this.state.members}
                    renderRow={(rowData) => <MemberRow />}
                    loadData={this.reloadArticles}
                    refreshDescription="Refreshing articles"
                />
            </View>
        </View>
        );
    }
}
