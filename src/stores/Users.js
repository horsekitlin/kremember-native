import Base from '../utils/StoreBase';

class UsersStore extends Base {
    constructor(name){
        super(name);
        this.login = this.login.bind(this);
    }
    login(resp){
        this.update('item', resp);
    }
}

var Users = new UsersStore('users');

export default Users;
