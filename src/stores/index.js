import events from 'events';
import Users from './Users';
import ClientSet from './ClientSet';

class EventEmitter extends events.EventEmitter{
    constructor(){
        super();
    }
    reloadComponent(name){
        this.emit(name);
    }
}

var Emitter = new EventEmitter();

export default {
    Notifier : Emitter,
    Users : Users,
    ClientSet : ClientSet,
};
