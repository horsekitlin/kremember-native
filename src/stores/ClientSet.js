import Base from '../utils/StoreBase';

class ClientSetStore extends Base {
    constructor(name){
        super(name);
        [ 'getview', 'redirect' ].map((key) => {
            this[key] = this[key].bind(this);
        }.bind(this));

        this._view = 'login';
    }
    getview(){
        return this._view;
    }
    redirect(view){
        console.log(view);
        this._view = view;
    }
}

var ClientSet = new ClientSetStore('setting');

export default ClientSet;
