import React from 'react-native';
const {
  Component,
  View
} = React;
import NavigationBar from 'react-native-navbar';
import CreateUser from './pages/createUser';

export default class LoginScreen extends Component {
    render() {
        const rightButtonConfig = {
            title: '建立會員',
            handler: () => this.props.navigator.push({
                component: CreateUser,
            }),
        };

        console.log(rightButtonConfig);
        return (
        <View style={{ flex: 1, backgroundColor: '#ff9900', }}>
            <NavigationBar
            title={{ title: 'Title', }}
            rightButton={rightButtonConfig} />
        </View>
        );
    }
}
