import React from 'react-native';
let {
    AsyncStorage,
} = React;

export default class StoreBase extends Object{

    constructor(name){
        super(name);
        if(AsyncStorage[name] === undefined){
            AsyncStorage.setItem(name, JSON.stringify({}));
        }
        [ 'update', 'commit' ].map((key) => {
            this[key] = this[key].bind(this);
        }.bind(this));

        this._db = AsyncStorage[name] || '{}';
        this._data = JSON.parse(this._db);
        this._name = name;
    }
    update(key, value){
        this._data[key] = value;
        this.commit();
    }
    commit(){
        AsyncStorage.setItem(this._name, JSON.stringify(this._data));
    }
}
