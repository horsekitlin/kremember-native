import { fetch_to, format_headers } from '../utils/requests';
import { Notifier, Member, Users } from '../stores';

class MemberBase extends Object {
    constructor(){
        super();
    }
    List(){
        var user = Users.detail();
        fetch_to('/member/list/v1/', {}, {
            token : user.token
        }).then((res) => {
            if(res.status === 200){
                Member.update('list', res.data.content);
                Notifier.reloadpage();
            }
        });
    }
    getDetail(data){
        let posts = this.List();

        let post = _.find(posts, (p) => {
            return p._id === data.member_id;
        });
        Member.update('item', post);
    }
}

var MemberManager = new MemberBase();

export default MemberManager;
