import { ClientSet, Notifier } from '../stores';

export default{
    Redirect : (data) => {
        ClientSet.redirect(data.view);
        Notifier.reloadComponent('ClientSet');
    },
};
