import Dispatcher from '../dispatcher';
import Constants from '../constants';
import Users from './Users';
import ClientSet from './ClientSet';
import { Notifier } from '../stores';

Notifier.dispatchToken = Dispatcher.register((evt) => {
    console.log(evt.action.data);
    switch(evt.action.actionType){
        case Constants.Users.FBLOGIN:
            Users.FBLogin(evt.action.data);
            break;
        case Constants.ClientSet.REDIRECT:
            ClientSet.Redirect(evt.action.data);
            break;
        case Constants.Users.LOGIN:
            Users.Login(evt.action.data);
            break;
        case Constants.Users.CREATEUSER:
            Users.Createuser(evt.action.data);
        break;
        default:
            console.log("Home");
    }
});

export default Notifier;
