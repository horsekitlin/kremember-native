import websetting from '../config';
import { fetch_to } from '../utils/requests';
import { Notifier, Users, ClientSet } from '../stores';

export default{
    Createuser : (data) => {
        fetch_to('/users/created/v1/', data)
        .then((json) => {
            if(json.status !== 200){
                alert(json.message);
            }else{
                ClientSet.redirect('login');
                Notifier.reloadComponent('ClientSet');
            }
        });
    },
    FBLogin : (data) => {
        fetch_to('/users/login/v1/', {
            fb_id : data.credentials.userId
        }).then((json) => {
            if(json.status === 200){
                data.token = json.data.token;
                data.fb_token = data.credentials.token;
                Users.update('user', json);
                ClientSet.redirect('member');
                Notifier.reloadComponent('LoginSuccess');
            }else if(json.status === 521){
                let userdata = {
                    account : data.credentials.userId,
                    pwd : '',
                    name : data.credentials.userId,
                    fb_id : data.credentials.userId,
                };
                fetch_to('/users/created/v1/', userdata)
                .then(() => {
                    fetch_to('/users/login/v1/', {
                        fb_id : data.credentials.userId
                    })
                    .then((json) => {
                        if(json.status !== 200){
                            alert(json.message);
                        }else{
                            data.token = json.data.token;
                            data.fb_token = data.credentials.token;
                            Users.update('user', json);
                            ClientSet.redirect('member');
                            Notifier.reloadComponent('LoginSuccess');
                        }
                    }).catch((err) => {
                        alert(err);
                    });
                });
            }else{
                alert(json.message);
            }
        }).catch((err) => {
            alert(err);
        });
    },
    Login : (data) => {
        fetch_to('/users/login/v1/', data)
        .then((json) => {
            if(json.status !== 200){
                alert(json.message);
            }else{
                data.token = json.data.token;
                Users.update('user', json);
                ClientSet.redirect('member');
                Notifier.reloadComponent('LoginSuccess');
            }
        }).catch((err) => {
            alert(err);
        });
    },
};
