import keyMirror from 'keymirror'

const Constants = {
    REDIRECT : '頁面導向'
};

export default keyMirror(Constants);

