import keyMirror from 'keymirror'

const Constants = {
    LOGIN : '帳號登入',
    FBLOGIN : 'FB登入',
    CREATEUSER : '建立會員',
};

export default keyMirror(Constants);

