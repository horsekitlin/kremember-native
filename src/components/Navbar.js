import React from 'react-native';
import NavigationBar from 'react-native-navbar';

import Pickachu from './Pickachu';
import Charmander from './Charmander';
import Bulbazavr from './Bulbazavr';

const {
  Component,
  Image,
  TouchableOpacity
} = React;

export default class NavBar extends Component {
  render() {
    const charmander = 'http://oyster.ignimgs.com/mediawiki/apis.ign.com/pokemon-blue-version/d/d4/Charmander.gif';
    return (
        <NavigationBar
          title={<Pickachu
                onPress={() => {alert('picachu');}}/>}
          leftButton={<Charmander
                style={{marginLeft : 8}}
                onPress={() => { alert('Charmanderrrr!!')}}/>}
          rightButton={<Bulbazavr
                style={{marginLeft : 8}}
                onPress={() => {alert('Bulbazavrrrrr!!!');}}/>} />
    );
  }
}
