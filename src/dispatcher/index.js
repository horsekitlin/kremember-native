import constants from '../constants';
import Flux from 'flux';

class Dispatcher extends Flux.Dispatcher {
    constructor(){
        super();
    }
    handleUsersAction(action){
        var payload = {
            source: 'Users',
            action: action
        };
        this.dispatch(payload);
    }
    handleMemberAction(action){
        var payload = {
            source: 'Member',
            action: action
        };
        this.dispatch(payload);
    }
    handleViewAction(action){
        console.log('view action');
        var payload = {
            source: 'View',
            action: action
        };
        this.dispatch(payload);
    }
}
var AppDispatcher = new Dispatcher();

export default AppDispatcher;

