import Dispatcher from '../dispatcher';
import Constants from '../constants';

export default {
    FBLogin : (data) => {
        Dispatcher.handleUsersAction({
            actionType : Constants.Users.FBLOGIN,
            data : data
        });
    },
    Login : (data) => {
        Dispatcher.handleUsersAction({
            actionType : Constants.Users.LOGIN,
            data : data
        });
    },
    Createuser : (data) => {
        Dispatcher.handleUsersAction({
            actionType : Constants.Users.CREATEUSER,
            data : data
        });
    }
};
