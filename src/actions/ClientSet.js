import Dispatcher from '../dispatcher';
import Constants from '../constants';

export default {
    Redirect : (data) => {
        console.log(Dispatcher.handleViewAction);
        Dispatcher.handleViewAction({
            actionType : Constants.ClientSet.REDIRECT,
            data : data
        });
    }
};
