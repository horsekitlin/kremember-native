import Dispatcher from '../dispatcher';
import Constants from '../constants';

export default {
    Redirect : (data) => {
        Dispatcher.handleViewAction({
            actionType : Constants.ClientSet.REDIRECT,
            data : data
        });
    }
};
